from django.urls import path
from game_sales import views

urlpatterns = [
    path('upload/', views.upload_data, name='upload'),
    path('filter/<filter_type>', views.FilterView.as_view(), name='filter'),
    path('order/<order_type>', views.OrderByView.as_view(), name='filter'),
]
