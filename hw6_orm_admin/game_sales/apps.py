from django.apps import AppConfig


class GamesalesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'game_sales'
