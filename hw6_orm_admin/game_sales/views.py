import csv
import datetime

from game_sales.models import GameModel
from django.http import HttpResponse
from django.views.generic import TemplateView, ListView

from django.shortcuts import render


def upload_data(request):
    count_needed = 1000
    count = 0
    with open('vgsales.csv') as f:
        reader = csv.reader(f)
        for row in reader:
            print(row)
            try:
                created = GameModel.objects.get_or_create(
                    name=row[1],
                    platform=row[2],
                    year=datetime.date(int(row[3]), 1, 1),
                    genre=row[4],
                    publisher=row[5],
                    global_sales=row[10],
                )
            except:
                pass
            count += 1
            if count >= count_needed:
                break

    return HttpResponse("Uploading done!")


class FilterView(TemplateView):
    template_name = 'game_model_filter.html'

    def get(self, request, filter_type):
        filter_types_dict = {
            '1': 'All sets',
            '2': 'Name contains "Hitman"',
            '3': 'Name exclude "Hitman"',
            '4': 'Name contains "Hitman" or "Tetris"',
            '5': 'Name contains "Hitman" or "Tetris" and Platform contains "PS"',
            '6': 'Publisher in ["JVC", "Square Enix"]',
            '7': 'Year less then or equal 2000',
            '8': 'Year in range [1998, 2002]',
            '9': 'Sales in range [3, 6]',
            '10': 'Name start with "As" & end with "s"',
            '11': 'Name start with "L"',
            '12': 'Name contains digit',
        }
        filter_name = filter_types_dict[filter_type]
        if filter_type == '1':
            game_list = GameModel.objects.all()
        elif filter_type == '2':
            game_list = GameModel.objects.filter(name__contains='Hitman')
        elif filter_type == '3':
            game_list = GameModel.objects.exclude(name__contains='Hitman')
        elif filter_type == '4':
            game_list = GameModel.objects.filter(name__contains='Hitman').union(
                GameModel.objects.filter(name__contains='Tetris'))
        elif filter_type == '5':
            game_list = (GameModel.objects.filter(name__contains='Hitman') |
                         GameModel.objects.filter(name__contains='Tetris')) & \
                        GameModel.objects.filter(platform__contains='PS')
        elif filter_type == '6':
            game_list = GameModel.objects.filter(publisher__in=["JVC", "Square Enix"])
        elif filter_type == '7':
            game_list = GameModel.objects.filter(year__lte=datetime.date(2000, 1, 1))
        elif filter_type == '8':
            game_list = GameModel.objects.filter(year__year__range=[1998, 2002])
        elif filter_type == '9':
            game_list = GameModel.objects.filter(global_sales__range=[3, 6])
        elif filter_type == '10':
            game_list = GameModel.objects.filter(name__startswith='As', name__endswith='s')
        elif filter_type == '11':
            game_list = GameModel.objects.filter(name__startswith='L')
        elif filter_type == '12':
            game_list = GameModel.objects.filter(name__regex=r'.*\d.*').order_by('year')

        ctx = {
            'game_list': game_list,
            'filter_name': filter_name,
            'length': len(game_list),
            'filter_types': filter_types_dict.items(),
        }
        return render(request, self.template_name, ctx)


class OrderByView(TemplateView):
    template_name = 'game_model_order.html'

    def get(self, request, order_type):
        order_types_dict = {
            '1': 'Order by Name',
            '2': 'Order by Year & some regex filter',
            '3': 'Order by Sales reverse & some regex exclude',
            '4': 'Order by Publisher reverse & some regex filter',
        }
        order_name = order_types_dict[order_type]
        if order_type == '1':
            game_list = GameModel.objects.all().order_by('name')
        elif order_type == '2':
            game_list = GameModel.objects.filter(name__regex=r'^(An? |The ).*').order_by('year')
        elif order_type == '3':
            game_list = GameModel.objects.exclude(name__regex=r'^(An?|The) .*').order_by('-global_sales')
        elif order_type == '4':
            game_list = GameModel.objects.filter(name__regex=r'^[^(An? |The )].*').order_by('publisher').reverse()

        ctx = {
            'game_list': game_list,
            'order_name': order_name,
            'length': len(game_list),
            'order_types': order_types_dict.items(),
        }
        return render(request, self.template_name, ctx)
